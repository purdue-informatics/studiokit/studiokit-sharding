﻿using Microsoft.Data.SqlClient;
using StudioKit.Configuration;
using StudioKit.Data.Utils;
using StudioKit.Encryption;
using System;

namespace StudioKit.Sharding;

/// <summary>
/// Provides access to appSettings, and contains advanced configuration settings.
/// </summary>
public static class ShardConfiguration
{
	public static string Tier { get; set; }

	public static string UserId { get; set; }

	public static string Password { get; set; }

	/// <summary>
	/// Gets a configuration setting, using Tier as a prefix. If prefixed value does not exist, gets the non-prefixed value.
	/// </summary>
	/// <param name="settingKey">The configuration setting key.</param>
	/// <returns>The configuration setting value.</returns>
	private static string GetTierSetting(string settingKey) =>
		EncryptedConfigurationManager.GetSetting($"{(!string.IsNullOrWhiteSpace(Tier) ? $"{Tier}_" : "")}{settingKey}") ??
		EncryptedConfigurationManager.GetSetting(settingKey);

	private static string _applicationName;

	/// <summary>
	/// Gets the name of the application.
	/// </summary>
	public static string ApplicationName
	{
		get
		{
			return _applicationName ?? GetTierSetting(BaseAppSetting.ApplicationName);
		}
		set
		{
			_applicationName = value;
		}
	}

	/// <summary>
	/// Gets the server name for the Shard Map Manager database, which contains the shard maps.
	/// </summary>
	public static string ShardMapManagerServerName => ShardServerName;

	private static string _shardMapManagerDatabaseSettingName;

	/// <summary>
	/// Gets the database name for the Shard Map Manager database, which contains the shard maps.
	/// </summary>
	public static string ShardMapManagerDatabaseSettingName
	{
		get
		{
			return _shardMapManagerDatabaseSettingName ?? GetTierSetting(ShardAppSetting.ShardMapManagerDatabaseName);
		}
		set
		{
			_shardMapManagerDatabaseSettingName = value;
		}
	}

	/// <summary>
	/// Gets the database name for the Shard Map Manager database, which contains the shard maps.
	/// </summary>
	public static string ShardMapManagerDatabaseName =>
		$"{ApplicationName}-{ShardMapManagerDatabaseSettingName}";

	/// <summary>
	/// Gets the name for the Shard Map that contains metadata for all the shards and the mappings to those shards.
	/// </summary>
	public static string ShardMapName => "CustomerShardMap";

	private static string _shardServerName;

	/// <summary>
	/// Gets the server name from the App.config file for shards to be created on.
	/// </summary>
	public static string ShardServerName
	{
		get
		{
			return _shardServerName ?? GetTierSetting(ShardAppSetting.SqlServerName);
		}
		set
		{
			_shardServerName = value;
		}
	}

	private static string _shardMapManagerDatabaseEdition;

	/// <summary>
	/// Gets the edition to use for Shard Map Manager Database if the server is an Azure SQL DB server.
	/// If the server is a regular SQL Server then this is ignored.
	/// </summary>
	public static string ShardMapManagerDatabaseEdition
	{
		get
		{
			return _shardMapManagerDatabaseEdition ?? GetTierSetting(ShardAppSetting.ShardMapManagerDatabaseEdition);
		}
		set
		{
			_shardMapManagerDatabaseEdition = value;
		}
	}

	private static string _shardDatabaseEdition;

	/// <summary>
	/// Gets the edition to use for Shards if the server is an Azure SQL DB server.
	/// If the server is a regular SQL Server then this is ignored.
	/// </summary>
	public static string ShardDatabaseEdition
	{
		get
		{
			return _shardDatabaseEdition ?? GetTierSetting(ShardAppSetting.ShardDatabaseEdition);
		}
		set
		{
			_shardDatabaseEdition = value;
		}
	}

	/// <summary>
	/// Format to use for creating shard name. {0} is the customer name. Use ToShardDatabaseName().
	/// </summary>
	public static string ShardDatabaseNameFormat => $"{ApplicationName}-Shard-{{0}}";

	/// <summary>
	/// Returns a connection string that can be used to connect to the specified server and database.
	/// </summary>
	public static string GetConnectionString(string serverName, string database)
	{
		var connectionStringBuilder = new SqlConnectionStringBuilder(GetCredentialsConnectionString(serverName))
		{
			DataSource = serverName,
			InitialCatalog = database
		};
		var connectionString = connectionStringBuilder.ToString();
		return connectionString;
	}

	/// <summary>
	/// Returns a connection string to use for Data-Dependent Routing and Multi-Shard Query,
	/// which does not contain DataSource or InitialCatalog.
	/// </summary>
	public static string GetCredentialsConnectionString(string serverName = null)
	{
		// use locally defined Tier for console app, or fallback to appSetting
		var tier = Tier ?? EncryptedConfigurationManager.GetSetting(BaseAppSetting.Tier);
		var shardServerName = serverName ?? ShardServerName;

		string userId;
		string password;
		bool integratedSecurity;
		SqlAuthenticationMethod sqlAuthenticationMethod;

		if (tier == Configuration.Tier.Local && SqlLocalDbUtils.IsLocalDb(shardServerName))
		{
			userId = string.Empty;
			password = string.Empty;
			integratedSecurity = true;
			sqlAuthenticationMethod = SqlAuthenticationMethod.NotSpecified;
		}
		else
		{
			// use locally defined variables if available, otherwise use appSettings
			userId = UserId ?? GetTierSetting(ShardAppSetting.SqlUserName);
			password = Password ?? GetTierSetting(ShardAppSetting.SqlPassword);
			if (string.IsNullOrWhiteSpace(userId))
				throw new NullReferenceException(nameof(userId));
			integratedSecurity = false;
			// override authentication method if using locally defined credentials
			sqlAuthenticationMethod =
				!string.IsNullOrWhiteSpace(userId) &&
				string.IsNullOrWhiteSpace(password) &&
				userId.Contains("purdue.edu")
					? SqlAuthenticationMethod.ActiveDirectoryInteractive
					: !string.IsNullOrWhiteSpace(userId) &&
					!string.IsNullOrWhiteSpace(password) &&
					userId.Contains("@purdue.edu")
						? SqlAuthenticationMethod.ActiveDirectoryPassword
						: SqlAuthenticationMethod.NotSpecified;
		}

		var maxPoolSizeString = GetTierSetting(ShardAppSetting.SqlMaxPoolSize);
		var maxPoolSize = !string.IsNullOrWhiteSpace(maxPoolSizeString)
			? int.Parse(maxPoolSizeString)
			: 100;

		var connectTimeoutString = GetTierSetting(ShardAppSetting.SqlConnectTimeout);
		var connectTimeout = !string.IsNullOrWhiteSpace(connectTimeoutString)
			? int.Parse(connectTimeoutString)
			: 30;

		var connectionStringBuilder = new SqlConnectionStringBuilder
		{
			// DDR and MSQ require credentials to be set
			UserID = userId,
			Password = password,
			IntegratedSecurity = integratedSecurity,
			Authentication = sqlAuthenticationMethod,

			// DataSource and InitialCatalog cannot be set for DDR and MSQ APIs, because these APIs will
			// determine the DataSource and InitialCatalog for you.
			//
			// DDR also does not support the ConnectRetryCount keyword introduced in .NET 4.5.1, because it
			// would prevent the API from being able to correctly kill connections when mappings are switched
			// offline.
			//
			// Other SqlClient ConnectionString keywords are supported.

			ApplicationName = ApplicationName,
			ConnectTimeout = connectTimeout,
			MaxPoolSize = maxPoolSize
		};

		if (tier == Configuration.Tier.Local && !SqlLocalDbUtils.IsLocalDb(shardServerName))
		{
			connectionStringBuilder.TrustServerCertificate = true;
		}

		return connectionStringBuilder.ToString();
	}
}