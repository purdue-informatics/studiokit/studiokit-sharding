using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace StudioKit.Sharding.Extensions;

public static class StringExtensions
{
	/// <summary>
	/// Assuming the string is a customer name, creates a camel-cased database name, removes "." and " " characters.
	/// </summary>
	/// <param name="customerName">The customer name, e.g. "Purdue"</param>
	/// <param name="databaseNameFormat">The database name format, e.g. "Pattern-Shard-{0}"</param>
	/// <returns></returns>
	public static string ToShardDatabaseName(this string customerName, string databaseNameFormat)
	{
		customerName = customerName.Replace(".", " ").Replace("-", " ");

		var databaseName = string.Format(databaseNameFormat,
			string.Join("",
				customerName.Split(' ')
					.Select(s => s.First().ToString().ToUpper() + string.Join("", s.Skip(1)))));
		return databaseName;
	}

	/// <summary>
	/// Assuming the string is a customer name, converts to a normalized format for use as a shard key.
	/// </summary>
	/// <param name="customerName">The customer name</param>
	/// <returns>The formatted name</returns>
	public static string ToShardKey(this string customerName)
	{
		return customerName.Replace(" ", "-").ToLowerInvariant();
	}

	/// <summary>
	/// Assuming the string is a shard key, converts to a byte array to be used to create a point mapping.
	/// </summary>
	/// <param name="shardKey">The shard key</param>
	/// <returns>The byte array shard key</returns>
	public static byte[] ToPointMapping(this string shardKey)
	{
		return Encoding.UTF8.GetBytes(shardKey);
	}

	/// <summary>
	/// Convert a subdomain to a shard key. Removes ".dev" or ".qa" from the end of the subdomain.
	/// </summary>
	/// <param name="subdomain">The subdomain</param>
	/// <returns>The customer key</returns>
	public static string FromSubdomainToShardKey(this string subdomain)
	{
		return Regex.Replace(subdomain, @"\.(dev|qa)$", "");
	}
}