﻿using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement;

namespace StudioKit.Sharding.Extensions;

public static class ShardLocationExtensions
{
	public static string GetConnectionString(this ShardLocation location)
	{
		return ShardConfiguration.GetConnectionString(location.Server, location.Database);
	}
}