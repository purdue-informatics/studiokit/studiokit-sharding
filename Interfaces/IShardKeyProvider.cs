﻿namespace StudioKit.Sharding.Interfaces;

public interface IShardKeyProvider
{
	string GetShardKey(bool shouldThrowIfNullOrWhitespace = true);
}