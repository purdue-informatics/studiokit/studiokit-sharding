﻿namespace StudioKit.Sharding.Interfaces;

public interface IBaseShardConfiguration
{
	string Name { get; set; }

	string DefaultTimeZoneId { get; set; }

	string RedisConnectionString { get; set; }

	bool RedisShouldCollectStatistics { get; set; }
}