﻿# StudioKit.Sharding

Classes for managing Shard Databases.

## Implementation

1. Create a subclass of `ShardManager` and create a static configure method.

```
// Setup Shard Domain
ShardDomainConfiguration.Instance = new ShardDomainConfiguration("app.org");

// Instantiate the ShardManager, Shard Map Manager and ShardMap at StartUp
var applicationShardManager = new ApplicationShardManager();
ShardManager.Instance = applicationShardManager;

// set up ShardMapDb and ShardMap if nameOrConnectionString is null or not a connection string (e.g. it is a shardKey)
// we don't want to use the ShardMap if we are explicitly connecting to a single database e.g. LINQPad or EF Core CLI
if (string.IsNullOrWhiteSpace(nameOrConnectionString) || !nameOrConnectionString.Contains("Data Source="))
{
	applicationShardManager.CreateOrGetShardMapDbAndManager();
	applicationShardManager.CreateOrGetDefaultShardMap();
}
```

2. In your `Startup.cs`, call the configure method.

### ShardConfiguration

The following appsettings will be used to construct the connection string for authentication methods

* `TIER`
* `ApplicationName`
* `SqlServerName`
* `SqlMaxPoolSize`: default is `100`
* `SqlConnectTimeout`: default is `30`

#### LOCAL Tier w/ LocalDb

When `TIER` is set to `LOCAL` and LocalDb is supported by the current machine, LocalDb will be used.

#### SqlPassword

The app will use SqlPassword authentication,
see [SQL Server Setup](https://gitlab.com/groups/purdue-informatics/-/wikis/Configuration/SQL-Server-Setup).

The following appsettings are used for this authentication method

* `SqlUserName`
* `SqlPassword`

## Console App

Create a console application project in order to use the shard manager commands. You must have all required settings in appsettings.json.

```
using DataAccess;
using Microsoft.Extensions.Configuration;
using StudioKit.Encryption;
using System;
using System.IO;

namespace ShardManager;

public class Program
{
	[STAThread] // enable open file dialog
	public static void Main()
	{
		var config = new ConfigurationBuilder()
			.SetBasePath(Directory.GetCurrentDirectory())
			.AddJsonFile("appsettings.json", true, true)
			.AddEnvironmentVariables()
			.Build();
		EncryptedConfigurationManager.Configuration = config;
		new ApplicationShardManager().StartConsoleApp();
	}
}
```

### Authentication - Active Directory

To connect to DBs using a ShardManager Console App and MFA is enabled in AD, you must use `ActiveDirectoryInteractive` credentials. (
Alternately, you can also use the app SqlPassword
credentials if needed.)

* `AD UserName`:
	* The fully qualified user name from AD in the Azure Portal, e.g. `alias_purdue.edu#EXT#@itapinformaticsazurelistspu.onmicrosoft.com`
* `Password`: leave blank

**Note:**

* If MFA is _not_ enabled in AD, then `ActiveDirectoryPassword` will be used with your **Purdue Email** and **Password**.
