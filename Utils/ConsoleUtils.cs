using System;
using System.ComponentModel;

namespace StudioKit.Sharding.Utils;

public static class ConsoleUtils
{
	/// <summary>
	/// Writes good information to the console.
	/// </summary>
	public static void WriteInfo(string format, params object[] args)
	{
		WriteColor(ConsoleColor.Green, format, args);
	}

	/// <summary>
	/// Writes detailed information to the console.
	/// </summary>
	public static void WriteDetailedInfo(string format, params object[] args)
	{
		WriteColor(ConsoleColor.DarkGray, "\t" + format, args);
	}

	/// <summary>
	/// Writes warning text to the console.
	/// </summary>
	public static void WriteWarning(string format, params object[] args)
	{
		WriteColor(ConsoleColor.Yellow, format, args);
	}

	/// <summary>
	/// Writes colored text to the console.
	/// </summary>
	public static void WriteColor(ConsoleColor color, string format, params object[] args)
	{
		var oldColor = Console.ForegroundColor;
		Console.ForegroundColor = color;
		Console.WriteLine(format, args);
		Console.ForegroundColor = oldColor;
	}

	/// <summary>
	/// Reads an integer from the console.
	/// </summary>
	public static int ReadIntegerInput(string prompt)
	{
		// this is never null since we sent false to allowNull param
		// ReSharper disable once PossibleInvalidOperationException
		return ReadIntegerInput(prompt, false).Value;
	}

	/// <summary>
	/// Reads an integer from the console, or returns null if the user enters nothing and allowNull is true.
	/// </summary>
	public static int? ReadIntegerInput(string prompt, bool allowNull)
	{
		while (true)
		{
			Console.Write(prompt);
			var line = Console.ReadLine();

			if (string.IsNullOrWhiteSpace(line) && allowNull)
			{
				return null;
			}

			if (int.TryParse(line, out var inputValue))
			{
				return inputValue;
			}
		}
	}

	/// <summary>
	/// Reads an integer from the console.
	/// </summary>
	public static int ReadIntegerInput(string prompt, int defaultValue, Func<int, bool> validator)
	{
		while (true)
		{
			var input = ReadIntegerInput(prompt, true);

			if (!input.HasValue)
			{
				// No input, so return default
				return defaultValue;
			}

			// Input was provided, so validate it
			if (validator(input.Value))
			{
				// Validation passed, so return
				return input.Value;
			}
		}
	}

	/// <summary>
	/// Writes all properties of the object to the console on a new line.
	/// </summary>
	/// <param name="obj">The object</param>
	public static void WriteObjectProperties(object obj)
	{
		foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
		{
			var name = descriptor.Name;
			var value = descriptor.GetValue(obj);
			WriteDetailedInfo("{0} = {1}", name, value);
		}

		Console.WriteLine();
	}
}