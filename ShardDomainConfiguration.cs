﻿using StudioKit.Configuration;
using StudioKit.Encryption;
using System;
using System.Collections.Generic;

namespace StudioKit.Sharding;

public class ShardDomainConfiguration
{
	public ShardDomainConfiguration()
	{
		RootDomainParts = new List<string> { "www", "dev", "qa" };
	}

	public ShardDomainConfiguration(string hostname) : this()
	{
		if (string.IsNullOrWhiteSpace(hostname))
			throw new ArgumentNullException(nameof(hostname));
		Hostname = hostname;
		RootDomainParts.Add(hostname.Split('.')[0]);
	}

	public string Hostname { get; }

	public List<string> RootDomainParts { get; set; }

	/// <summary>
	/// Given a shard key, returns the URL where the API or Web is hosted.
	/// If <see cref="ShardAppSetting.ApiEndpointUri"/> or <see cref="ShardAppSetting.WebEndpointUri"/> are defined in app settings,
	/// these values will be returned, and are required when running locally.
	/// </summary>
	/// <param name="shardKey">The shard key</param>
	/// <param name="shouldReturnClientBaseUrl">When true, returns the URL, by convention, where the frontend is hosted.</param>
	/// <param name="tier">Optional override to request the base URL for a specific tier other than <see cref="BaseAppSetting.Tier"/>.</param>
	/// <param name="shouldIgnoreEndpointOverrides">Optional argument to ignore the overrides provided by app settings.</param>
	/// <returns></returns>
	public string BaseUrlWithShardKey(string shardKey, bool shouldReturnClientBaseUrl = false, string tier = null, bool shouldIgnoreEndpointOverrides = false)
	{
		// In unit testing scenarios, hostname will be null. And there will be no need for a base url
		if (Hostname == null)
			return null;

		var targetTier = tier ?? EncryptedConfigurationManager.GetSetting(BaseAppSetting.Tier);
		var webEndpointUri = EncryptedConfigurationManager.GetSetting(ShardAppSetting.WebEndpointUri);
		var apiEndpointUri = EncryptedConfigurationManager.GetSetting(ShardAppSetting.ApiEndpointUri);

		// endpoint app settings are required when running locally
		if (targetTier == Tier.Local && shouldReturnClientBaseUrl && string.IsNullOrWhiteSpace(webEndpointUri))
			throw new Exception($"{ShardAppSetting.WebEndpointUri} needs to be defined in configuration");
		if (targetTier == Tier.Local && !shouldReturnClientBaseUrl && string.IsNullOrWhiteSpace(apiEndpointUri))
			throw new Exception($"{ShardAppSetting.ApiEndpointUri} needs to be defined in configuration");

		// use endpoint app settings if they exist, e.g. on local or staging, where the standard hostname does not work
		if (!shouldIgnoreEndpointOverrides && shouldReturnClientBaseUrl && !string.IsNullOrWhiteSpace(webEndpointUri))
			return webEndpointUri;
		if (!shouldIgnoreEndpointOverrides && !shouldReturnClientBaseUrl && !string.IsNullOrWhiteSpace(apiEndpointUri))
			return apiEndpointUri;

		// construct the base url using the shard key, tier, and api subdomain parts, if applicable
		var hostname = shouldReturnClientBaseUrl ? Hostname.Replace("api.", "") : Hostname;
		return $"https://{shardKey}{GetTierSubdomainPart(targetTier)}.{hostname}";
	}

	/// <summary>
	/// If DEV or QA, returns the tier subdomain part, e.g. ".dev"
	/// </summary>
	public static string GetTierSubdomainPart(string tier) => tier switch
	{
		Tier.QA => ".qa",
		Tier.Dev => ".dev",
		_ => ""
	};

	#region Singleton

	private static ShardDomainConfiguration _instance;

	public static ShardDomainConfiguration Instance
	{
		get => _instance ?? (_instance = new ShardDomainConfiguration());
		set => _instance = value;
	}

	#endregion Singleton
}