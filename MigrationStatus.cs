﻿using System.Collections.Generic;

namespace StudioKit.Sharding;

public class MigrationStatus
{
	public string Database { get; set; }

	public List<string> CurrentMigrations { get; set; }

	public List<string> PendingMigrations { get; set; }
}