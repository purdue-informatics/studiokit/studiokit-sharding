using StudioKit.Data;
using StudioKit.Sharding.Interfaces;

namespace StudioKit.Sharding.Models;

public class BaseShardConfiguration : ModelBase, IBaseShardConfiguration
{
	public string Name { get; set; }

	public string DefaultTimeZoneId { get; set; }

	public string RedisConnectionString { get; set; }

	public bool RedisShouldCollectStatistics { get; set; }
}