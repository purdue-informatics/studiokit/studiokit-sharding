﻿namespace StudioKit.Sharding.Models;

public class Organization
{
	public string Name { get; set; }

	public string Url { get; set; }

	public byte[] Image { get; set; }
}