﻿using StudioKit.Data;
using StudioKit.Sharding.Interfaces;
using System;

namespace StudioKit.Sharding.Models;

public class BaseLicense : ModelBase, IBaseLicense
{
	public DateTime StartDate { get; set; }

	public DateTime EndDate { get; set; }
}