﻿using FluentValidation;
using StudioKit.Sharding.Interfaces;

namespace StudioKit.Sharding;

public class SimpleShardKeyProvider : IShardKeyProvider
{
	private readonly string _shardKey;

	public SimpleShardKeyProvider(string shardKey)
	{
		_shardKey = shardKey;
	}

	public string GetShardKey(bool shouldThrowIfNullOrWhitespace = true)
	{
		if (string.IsNullOrWhiteSpace(_shardKey) && shouldThrowIfNullOrWhitespace)
			throw new ValidationException(Properties.Strings.ShardKeyRequired);
		return _shardKey;
	}
}