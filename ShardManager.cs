﻿using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement;
using Microsoft.Extensions.Logging;
using StudioKit.Configuration;
using StudioKit.Encryption;
using StudioKit.Sharding.Extensions;
using StudioKit.Sharding.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Sharding;

public class ShardManager
{
	public ShardMapManager ShardMapManger { get; private set; }

	public ListShardMap<byte[]> DefaultShardMap { get; private set; }

	protected string CustomerName;
	protected string ShardKey;
	protected bool IsConsole;
	protected readonly ILoggerFactory LoggerFactory;
	protected readonly ILogger<ShardManager> Logger;

	protected ShardManager(ILoggerFactory loggerFactory, ILogger<ShardManager> logger)
	{
		IsConsole = false;
		LoggerFactory = loggerFactory;
		Logger = logger;
	}

	#region Singleton

	public static ShardManager Instance { get; protected set; }

	#endregion Singleton

	protected string Subdomain => $"{ShardKey}{ShardDomainConfiguration.GetTierSubdomainPart(ShardConfiguration.Tier)}";

	/// <summary>
	/// Creates a shard map manager, its database, and a shard map.
	/// </summary>
	public ShardMapManager CreateOrGetShardMapDbAndManager()
	{
		if (ShardMapManger != null)
		{
			OutputDetailedInfo("ShardMapManager exists");
			return ShardMapManger;
		}

		// Create shard map manager database
		if (!SqlDatabaseUtils.DatabaseExists(
				ShardConfiguration.ShardMapManagerServerName,
				ShardConfiguration.ShardMapManagerDatabaseName))
		{
			SqlDatabaseUtils.CreateDatabase(
				ShardConfiguration.ShardMapManagerServerName,
				ShardConfiguration.ShardMapManagerDatabaseName,
				ShardConfiguration.ShardMapManagerDatabaseEdition);
		}

		// Create shard map manager
		var shardMapManagerConnectionString =
			ShardConfiguration.GetConnectionString(
				ShardConfiguration.ShardMapManagerServerName,
				ShardConfiguration.ShardMapManagerDatabaseName);

		ShardMapManger = ShardUtils.CreateOrGetShardMapManager(shardMapManagerConnectionString, f => OutputDetailedInfo(f));

		return ShardMapManger;
	}

	public ListShardMap<byte[]> CreateOrGetDefaultShardMap()
	{
		if (DefaultShardMap != null)
		{
			OutputDetailedInfo("DefaultShardMap exists");
			return DefaultShardMap;
		}

		// Create shard map
		DefaultShardMap = ShardUtils.CreateOrGetListShardMap<byte[]>(
			ShardMapManger,
			ShardConfiguration.ShardMapName,
			(f, p) => OutputDetailedInfo(f, p));
		return DefaultShardMap;
	}

	public async Task<List<MigrationStatus>> GetAllShardDatabaseMigrationStatusesAsync(CancellationToken cancellationToken = default)
	{
		var results = new List<MigrationStatus>();

		var shardMap = TryGetShardMap();
		if (shardMap == null) return results;

		foreach (var shard in shardMap.GetShards())
		{
			var shardResult = await GetShardDatabaseMigrationStatusAsync(shard.Location, cancellationToken);
			results.Add(shardResult);
		}

		return results;
	}

	public async Task<List<MigrationStatus>> MigrateAndSeedAllShardDatabasesAsync(string targetMigration = null,
		CancellationToken cancellationToken = default)
	{
		var results = new List<MigrationStatus>();

		var shardMap = TryGetShardMap();
		if (shardMap == null) return results;

		foreach (var shard in shardMap.GetShards())
		{
			OutputInfo("Migrating \"{0}\" - \"{1}\"", shard.Location.Server, shard.Location.Database);
			var shardMigrationStatus = await MigrateShardDatabaseAsync(shard.Location, targetMigration, cancellationToken);
			results.Add(shardMigrationStatus);
			OutputInfo("Seeding \"{0}\" - \"{1}\"", shard.Location.Server, shard.Location.Database);
			await SeedShardDatabaseAsync(shard.Location, cancellationToken);
		}

		return results;
	}

	public async Task ConfigureAndSeedAllShardDatabasesAsync(CancellationToken cancellationToken = default)
	{
		var shardMap = TryGetShardMap();
		if (shardMap == null) return;

		foreach (var shard in shardMap.GetShards())
		{
			OutputInfo("Configuring \"{0}\" - \"{1}\"", shard.Location.Server, shard.Location.Database);
			await ConfigureShardDatabaseAsync(shard.Location, cancellationToken);
			OutputInfo("Seeding \"{0}\" - \"{1}\"", shard.Location.Server, shard.Location.Database);
			await SeedShardDatabaseAsync(shard.Location, cancellationToken);
		}
	}

	public async Task SeedAllShardDatabasesAsync(CancellationToken cancellationToken = default)
	{
		var shardMap = TryGetShardMap();
		if (shardMap == null) return;

		foreach (var shard in shardMap.GetShards())
		{
			OutputInfo("Seeding \"{0}\" - \"{1}\"", shard.Location.Server, shard.Location.Database);
			await SeedShardDatabaseAsync(shard.Location, cancellationToken);
		}
	}

	/// <summary>
	/// Override point for subclasses to run database migrations on a shard database.
	/// </summary>
	/// <param name="location">The location of the shard database.</param>
	/// <param name="targetMigration">Optional target migration, otherwise updates to latest migration.</param>
	/// <param name="cancellationToken"></param>
	protected virtual Task<MigrationStatus> MigrateShardDatabaseAsync(ShardLocation location, string targetMigration = null,
		CancellationToken cancellationToken = default)
	{
		throw new NotImplementedException();
	}

	/// <summary>
	/// Override point for subclasses to get database migrations on a shard database.
	/// </summary>
	/// <param name="location">The location of the shard database.</param>
	/// <param name="cancellationToken"></param>
	protected virtual Task<MigrationStatus> GetShardDatabaseMigrationStatusAsync(ShardLocation location,
		CancellationToken cancellationToken = default)
	{
		throw new NotImplementedException();
	}

	/// <summary>
	/// Override point for subclasses to run configure methods on a shard database.
	/// </summary>
	/// <param name="location">The location of the shard database. </param>
	/// <param name="cancellationToken"></param>
	protected virtual Task ConfigureShardDatabaseAsync(ShardLocation location, CancellationToken cancellationToken = default)
	{
		throw new NotImplementedException();
	}

	/// <summary>
	/// Override point for subclasses to run database seed methods on a shard database.
	/// </summary>
	/// <param name="location">The location of the shard database. </param>
	/// <param name="cancellationToken"></param>
	protected virtual Task SeedShardDatabaseAsync(ShardLocation location, CancellationToken cancellationToken = default)
	{
		throw new NotImplementedException();
	}

	#region Console App

	private const ConsoleColor EnabledColor = ConsoleColor.White;

	private const ConsoleColor DisabledColor = ConsoleColor.DarkGray;

	private enum MenuChoice
	{
		CreateShardMapManagerAndDatabase = 1,
		AddShard,
		MigrateAndSeedShard,
		ConfigureAndSeedShard,
		DropShard,
		MigrateAndSeedAll,
		ConfigureAndSeedAll,
		DropAll,
		ChangeTier,
		Exit
	}

	private static string MenuChoiceText(MenuChoice menuChoice)
	{
		return menuChoice switch
		{
			MenuChoice.CreateShardMapManagerAndDatabase => "Create shard map manager and database",
			MenuChoice.AddShard => "Add a new shard",
			MenuChoice.MigrateAndSeedShard => "Migrate and Seed a shard",
			MenuChoice.ConfigureAndSeedShard => "Configure and Seed a shard",
			MenuChoice.DropShard => "Drop a shard",
			MenuChoice.MigrateAndSeedAll => "Migrate and Seed all shards",
			MenuChoice.ConfigureAndSeedAll => "Configure and Seed all shards",
			MenuChoice.DropAll => "Drop all shards",
			MenuChoice.ChangeTier => "Change Tier",
			MenuChoice.Exit => "Exit",
			_ => throw new ArgumentOutOfRangeException(nameof(menuChoice), menuChoice, null)
		};
	}

	public async Task StartConsoleAppAsync(CancellationToken cancellationToken = default)
	{
		IsConsole = true;

		// Welcome screen
		Console.WriteLine("***********************************************************");
		Console.WriteLine("*******    Welcome to the {0} Shard Manager    ******", ShardConfiguration.ApplicationName);
		Console.WriteLine("***********************************************************");
		Console.WriteLine();

		// Prompt for Tier
		if (!SetAndConnectToTier()) return;

		// Connection succeeded. Begin interactive loop
		await MenuLoopAsync(cancellationToken);
	}

	public bool SetupCommandLine()
	{
		var tier = EncryptedConfigurationManager.GetSetting(BaseAppSetting.Tier);
		if (string.IsNullOrWhiteSpace(tier)) throw new ArgumentNullException(BaseAppSetting.Tier);
		if (tier != Tier.Local && tier != Tier.Dev && tier != Tier.QA && tier != Tier.Prod) throw new Exception("Invalid tier");
		if (string.IsNullOrWhiteSpace(ShardConfiguration.ApplicationName))
			throw new ArgumentNullException(BaseAppSetting.ApplicationName);
		if (string.IsNullOrWhiteSpace(ShardConfiguration.ShardMapManagerDatabaseSettingName))
			throw new ArgumentNullException(ShardAppSetting.ShardMapManagerDatabaseName);
		if (string.IsNullOrWhiteSpace(EncryptedConfigurationManager.GetSetting(ShardAppSetting.SqlServerName)))
			throw new ArgumentNullException(ShardAppSetting.SqlServerName);
		if (string.IsNullOrWhiteSpace(EncryptedConfigurationManager.GetSetting(ShardAppSetting.SqlUserName)))
			throw new ArgumentNullException(ShardAppSetting.SqlUserName);
		if (string.IsNullOrWhiteSpace(EncryptedConfigurationManager.GetSetting(ShardAppSetting.SqlPassword)))
			throw new ArgumentNullException(ShardAppSetting.SqlPassword);

		if (!ConfirmServerExists()) return false;
		SetShardMapManager();
		return true;
	}

	private static void PrintServer()
	{
		ConsoleUtils.WriteInfo("You are connected to the {0} database: \"{1}\"", ShardConfiguration.Tier,
			ShardConfiguration.ShardMapManagerServerName);
	}

	private static bool ConfirmServerExists()
	{
		// Verify that we can connect to the Sql Database that is specified in App.config settings
		if (SqlDatabaseUtils.TryConnectToSqlServer(ShardConfiguration.ShardMapManagerServerName))
		{
			return true;
		}

		// Connecting to the server failed - please update the settings in App.Config

		if (!Debugger.IsAttached) return false;

		// Give the user a chance to read the message, if this program is being run
		// in debug mode in Visual Studio
		Console.WriteLine("Press ENTER to continue...");
		Console.ReadLine();

		// Exit
		return false;
	}

	/// <summary>
	/// Main program loop.
	/// </summary>
	private async Task MenuLoopAsync(CancellationToken cancellationToken = default)
	{
		// Loop until the user chose "Exit".
		bool continueLoop;
		do
		{
			PrintShardMapState();
			Console.WriteLine();

			PrintMenu();
			Console.WriteLine();

			continueLoop = await GetMenuChoiceAndExecuteAsync(cancellationToken);
			Console.WriteLine();
		} while (continueLoop);
	}

	/// <summary>
	/// Writes the shard map's state to the console.
	/// </summary>
	private void PrintShardMapState()
	{
		PrintServer();

		Console.WriteLine("Current Shard Map state:");
		var shardMap = TryGetShardMap();
		if (shardMap == null)
		{
			return;
		}

		// Get all shards
		var allShards = shardMap.GetShards();

		// Get all mappings, grouped by the shard that they are on. We do this all in one go to minimize round trips.
		var mappingsGroupedByShard = shardMap.GetMappings().ToLookup(m => m.Shard);

		if (allShards.Any())
		{
			// The shard map contains some shards, so for each shard (sorted by database name)
			// write out the mappings for that shard
			foreach (var shard in shardMap.GetShards().OrderBy(s => s.Location.Database))
			{
				var mappingsOnThisShard = mappingsGroupedByShard[shard];

				// avoid multiple enumeration
				var mappingsArray = mappingsOnThisShard as PointMapping<byte[]>[] ?? mappingsOnThisShard.ToArray();

				if (mappingsArray.Any())
				{
					var mappingsString = string.Join(", ", mappingsArray.Select(m => Encoding.UTF8.GetString(m.Value)));
					Console.WriteLine("\t\"{0}\" contains point mappings \"{1}\"", shard.Location.Database, mappingsString);
				}
				else
				{
					Console.WriteLine("\t\"{0}\" contains no mappings.", shard.Location.Database);
				}
			}
		}
		else
		{
			Console.WriteLine("\tShard Map contains no shards");
		}
	}

	/// <summary>
	/// Writes the program menu.
	/// </summary>
	private void PrintMenu()
	{
		ConsoleColor createSmmColor; // color for create shard map manger menu item
		ConsoleColor otherMenuItemColor; // color for other menu items
		if (ShardMapManger == null)
		{
			createSmmColor = EnabledColor;
			otherMenuItemColor = DisabledColor;
		}
		else
		{
			createSmmColor = DisabledColor;
			otherMenuItemColor = EnabledColor;
		}

		foreach (MenuChoice menuChoice in Enum.GetValues(typeof(MenuChoice)))
		{
			ConsoleUtils.WriteColor(menuChoice == MenuChoice.CreateShardMapManagerAndDatabase
					? createSmmColor
					: menuChoice == MenuChoice.Exit
						? EnabledColor
						: otherMenuItemColor, $"{(int)menuChoice}. {MenuChoiceText(menuChoice)}");
		}
	}

	/// <summary>
	/// Gets the user's chosen menu item and executes it.
	/// </summary>
	/// <returns>true if the program should continue executing.</returns>
	private async Task<bool> GetMenuChoiceAndExecuteAsync(CancellationToken cancellationToken = default)
	{
		while (true)
		{
			var inputValue = (MenuChoice)ConsoleUtils.ReadIntegerInput($"Enter an option [1-{(int)MenuChoice.Exit}] and press ENTER: ");

			switch (inputValue)
			{
				case MenuChoice.CreateShardMapManagerAndDatabase:
					Console.WriteLine();
					CreateOrGetShardMapDbAndManager();
					CreateOrGetDefaultShardMap();
					return true;

				case MenuChoice.AddShard:
					Console.WriteLine();
					await ReadCustomerNameAndAddShardAsync(cancellationToken);
					return true;

				case MenuChoice.MigrateAndSeedShard:
					Console.WriteLine();
					await ChooseAndMigrateAndSeedShardAsync(cancellationToken);
					return true;

				case MenuChoice.ConfigureAndSeedShard:
					Console.WriteLine();
					await ChooseAndConfigureAndSeedShardAsync(cancellationToken);
					return true;

				case MenuChoice.DropShard:
					Console.WriteLine();
					ChooseAndDropShard();
					return true;

				case MenuChoice.MigrateAndSeedAll:
					Console.WriteLine();
					await ReadTargetMigrationAndMigrateAndSeedAllShardsAsync(cancellationToken);
					return true;

				case MenuChoice.ConfigureAndSeedAll:
					Console.WriteLine();
					await ConfigureAndSeedAllShardDatabasesAsync(cancellationToken);
					return true;

				case MenuChoice.DropAll:
					Console.WriteLine();
					ConfirmDropAll();
					return true;

				case MenuChoice.ChangeTier:
					Console.WriteLine();
					return SetAndConnectToTier();

				case MenuChoice.Exit:
					return false;
			}
		}
	}

	private static void SetTier()
	{
		while (true)
		{
			Console.Write("Enter a tier (LOCAL, DEV, QA, or PROD) and press ENTER: ");
			var tier = Console.ReadLine()?.ToUpper();
			if (string.IsNullOrWhiteSpace(tier) || tier != Tier.Local && tier != Tier.Dev && tier != Tier.QA && tier != Tier.Prod)
			{
				ConsoleUtils.WriteWarning("Invalid tier, try again");
				continue;
			}

			ShardConfiguration.Tier = tier;
			ConsoleUtils.WriteInfo("Tier set to \"{0}\"", tier);
			break;
		}
	}

	private bool SetAndConnectToTier()
	{
		SetTier();

		SetCredentials();

		if (!ConfirmServerExists()) return false;

		SetShardMapManager();

		return true;
	}

	private void SetShardMapManager()
	{
		// replace ShardMapManager after change
		ShardMapManger = ShardUtils.TryGetShardMapManager(
			ShardConfiguration.ShardMapManagerServerName,
			ShardConfiguration.ShardMapManagerDatabaseName);
	}

	private static void SetCredentials()
	{
		if (ShardConfiguration.Tier == Tier.Local) return;

		string userId;
		while (true)
		{
			Console.Write("Enter the DB UserId or AD UserName (see StudioKit.Sharding/README), and press ENTER: ");
			userId = Console.ReadLine();
			if (string.IsNullOrWhiteSpace(userId))
			{
				ConsoleUtils.WriteWarning("UserId required, try again");
				continue;
			}

			break;
		}

		var password = "";
		while (true)
		{
			Console.Write("Enter the DB Password or leave blank to use AD Interactive, and press ENTER: ");
			do
			{
				var key = Console.ReadKey(true);
				// Backspace Should Not Work
				if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
				{
					password += key.KeyChar;
					Console.Write("*");
				}
				else
				{
					if (key.Key == ConsoleKey.Backspace && !string.IsNullOrWhiteSpace(password))
					{
						password = password.Substring(0, (password.Length - 1));
						Console.Write("\b \b");
					}
					else if (key.Key == ConsoleKey.Enter)
					{
						break;
					}
				}
			} while (true);

			break;
		}

		ShardConfiguration.UserId = userId;
		ShardConfiguration.Password = password;

		Console.WriteLine();
		ConsoleUtils.WriteInfo("UserId and Password set");
	}

	/// <summary>
	/// Reads the user's choice of a customer name, and creates a new shard with a mapping for the resulting customer.
	/// </summary>
	private async Task ReadCustomerNameAndAddShardAsync(CancellationToken cancellationToken = default)
	{
		Console.Write("Enter a new customer name and press ENTER: ");
		var customerName = Console.ReadLine();
		if (string.IsNullOrWhiteSpace(customerName))
			return;

		Console.Write("Enter a new database name and press ENTER: ");
		var databaseName = Console.ReadLine().ToShardDatabaseName(ShardConfiguration.ShardDatabaseNameFormat);
		if (string.IsNullOrWhiteSpace(databaseName))
			return;

		Console.Write("Enter a new customer key and press ENTER: ");
		var customerKey = Console.ReadLine().ToShardKey();
		if (string.IsNullOrWhiteSpace(customerKey))
			return;

		Console.WriteLine();
		Console.WriteLine("Creating shard \"{0}\" for customer \"{1}\" with key \"{2}\"", databaseName, customerName, customerKey);

		await AddShardAsync(customerName, databaseName, customerKey, cancellationToken);
	}

	private async Task AddShardAsync(string customerName, string databaseName, string shardKey,
		CancellationToken cancellationToken = default)
	{
		var shardMap = TryGetShardMap();
		if (shardMap == null)
			return;

		var shardMapping = TryGetShardMappingByShardKey(shardKey);
		if (shardMapping != null)
		{
			ConsoleUtils.WriteWarning("ShardMapping already exists for shardKey = \"{0}\"", shardKey);
			return;
		}

		// store name and key on protected variables
		CustomerName = customerName;
		ShardKey = shardKey;

		// Create a new shard, or get an existing empty shard (if a previous create partially succeeded).
		var shard = ShardUtils.CreateOrGetEmptyShard(
			shardMap,
			databaseName,
			ShardConfiguration.ShardServerName,
			ShardConfiguration.ShardDatabaseEdition);

		// Create a mapping to that shard
		var mappingForNewShard = shardMap.CreatePointMapping(shardKey.ToPointMapping(), shard);
		ConsoleUtils.WriteDetailedInfo("Mapped point \"{0}\" to shard \"{1}\"",
			Encoding.UTF8.GetString(mappingForNewShard.Value), shard.Location.Database);

		await MigrateShardDatabaseAsync(shard.Location, cancellationToken: cancellationToken);
		await ConfigureShardDatabaseAsync(shard.Location, cancellationToken);
		await SeedShardDatabaseAsync(shard.Location, cancellationToken);

		// clear out creation variables
		CustomerName = null;
		ShardKey = null;
	}

	private async Task ChooseAndMigrateAndSeedShardAsync(CancellationToken cancellationToken = default)
	{
		var customerKey = ReadShardCustomerKey("migrate");
		if (string.IsNullOrWhiteSpace(customerKey))
			return;

		var targetMigration = ReadTargetMigration();

		var shardMapping = TryGetShardMappingByShardKey(customerKey);
		if (shardMapping == null)
		{
			ConsoleUtils.WriteWarning("Shard Mapping not found");
			return;
		}

		OutputInfo("Migrating \"{0}\" - \"{1}\"", shardMapping.Shard.Location.Server, shardMapping.Shard.Location.Database);
		await MigrateShardDatabaseAsync(shardMapping.Shard.Location, targetMigration, cancellationToken);
		OutputInfo("Seeding \"{0}\" - \"{1}\"", shardMapping.Shard.Location.Server, shardMapping.Shard.Location.Database);
		await SeedShardDatabaseAsync(shardMapping.Shard.Location, cancellationToken);
	}

	private async Task ReadTargetMigrationAndMigrateAndSeedAllShardsAsync(CancellationToken cancellationToken = default)
	{
		var targetMigration = ReadTargetMigration();
		await MigrateAndSeedAllShardDatabasesAsync(targetMigration, cancellationToken);
	}

	private async Task ChooseAndConfigureAndSeedShardAsync(CancellationToken cancellationToken = default)
	{
		var customerKey = ReadShardCustomerKey("seed");
		if (string.IsNullOrWhiteSpace(customerKey))
			return;

		var shardMapping = TryGetShardMappingByShardKey(customerKey);
		if (shardMapping == null)
		{
			ConsoleUtils.WriteWarning("Shard Mapping not found");
			return;
		}

		ShardKey = customerKey;
		OutputInfo("Configuring \"{0}\" - \"{1}\"", shardMapping.Shard.Location.Server, shardMapping.Shard.Location.Database);
		await ConfigureShardDatabaseAsync(shardMapping.Shard.Location, cancellationToken);
		OutputInfo("Seeding \"{0}\" - \"{1}\"", shardMapping.Shard.Location.Server, shardMapping.Shard.Location.Database);
		await SeedShardDatabaseAsync(shardMapping.Shard.Location, cancellationToken);
	}

	private void ChooseAndDropShard()
	{
		var customerKey = ReadShardCustomerKey("drop");
		if (string.IsNullOrWhiteSpace(customerKey))
			return;

		var shardMapping = TryGetShardMappingByShardKey(customerKey);
		if (shardMapping == null)
		{
			ConsoleUtils.WriteWarning("Shard Mapping not found");
			return;
		}

		DropShardDatabase(shardMapping);
	}

	/// <summary>
	/// Drop a specific shard database.
	/// </summary>
	/// <param name="shardMapping"></param>
	private void DropShardDatabase(PointMapping<byte[]> shardMapping)
	{
		var shard = shardMapping.Shard;
		var location = shard.Location;

		OutputInfo("Dropping Shard \"{0}\" - \"{1}\"", location.Server, location.Database);

		var shardMap = TryGetShardMap();
		if (shardMap == null) return;
		shardMapping = shardMap.MarkMappingOffline(shardMapping);
		shardMap.DeleteMapping(shardMapping);

		shard = ShardUtils.CreateOrGetShard(shardMap, location);
		shardMap.DeleteShard(shard);
		SqlDatabaseUtils.DropDatabase(location.DataSource, location.Database);
	}

	private void ConfirmDropAll()
	{
		Console.Write("Are you sure you want to DROP ALL DATABASES? (Y/N or leave blank to cancel) ");
		var response = Console.ReadLine();
		if (string.IsNullOrWhiteSpace(response) || response.ToUpperInvariant().Equals("N"))
			return;

		DropAll();
	}

	/// <summary>
	/// Drops all shards and the shard map manager database (if it exists).
	/// </summary>
	private void DropAll()
	{
		var shardMap = TryGetShardMap();
		if (shardMap != null)
		{
			foreach (var shard in shardMap.GetShards())
			{
				ConsoleUtils.WriteInfo("Dropping Shard \"{0}\" - \"{1}\"", shard.Location.Server, shard.Location.Database);
				SqlDatabaseUtils.DropDatabase(shard.Location.DataSource, shard.Location.Database);
			}
		}

		if (SqlDatabaseUtils.DatabaseExists(ShardConfiguration.ShardMapManagerServerName, ShardConfiguration.ShardMapManagerDatabaseName))
		{
			// Drop shard map manager database
			SqlDatabaseUtils.DropDatabase(ShardConfiguration.ShardMapManagerServerName, ShardConfiguration.ShardMapManagerDatabaseName);
		}

		// Since we just dropped the shard map manager database, this shardMapManager reference is now non-functional.
		// So set it to null so that the program knows that the shard map manager is gone.
		ShardMapManger = null;
	}

	private static string ReadShardCustomerKey(string verb)
	{
		Console.Write(
			$"Enter the customer key (e.g. \"purdue\") of the shard you wish to {verb} and press ENTER: (or leave blank to cancel) ");
		var customerKey = Console.ReadLine();
		return string.IsNullOrWhiteSpace(customerKey) ? null : customerKey;
	}

	private static string ReadTargetMigration()
	{
		Console.Write("Enter the target migration and press ENTER: (or leave blank to migrate to the newest migration) ");
		var targetMigration = Console.ReadLine();
		return string.IsNullOrWhiteSpace(targetMigration) ? null : targetMigration;
	}

	#endregion Console App

	#region Helpers

	/// <summary>
	/// Gets the shard map, if it exists. If it doesn't exist, writes out the reason and returns null.
	/// </summary>
	private ListShardMap<byte[]> TryGetShardMap()
	{
		if (ShardMapManger == null)
		{
			OutputWarning("Shard Map Manager has not yet been created");
			return null;
		}

		var shardMap = ShardUtils.TryGetListShardMap<byte[]>(ShardMapManger,
			ShardConfiguration.ShardMapName);

		if (shardMap != null)
			return shardMap;

		OutputWarning("Shard Map Manager has been created, but the Shard Map has not been created");

		return null;
	}

	private PointMapping<byte[]> TryGetShardMappingByShardKey(string customerKey)
	{
		var shardMap = TryGetShardMap();
		if (shardMap == null)
			return null;
		shardMap.TryGetMappingForKey(customerKey.ToPointMapping(), out var shardMapping);
		return shardMapping;
	}

	protected void OutputInfo(string format, params object[] args)
	{
		if (IsConsole) ConsoleUtils.WriteInfo(format, args);
		else Logger.LogInformation(format, args);
	}

	protected void OutputDetailedInfo(string format, params object[] args)
	{
		if (IsConsole) ConsoleUtils.WriteDetailedInfo(format, args);
		else Logger.LogInformation(format, args);
	}

	protected void OutputWarning(string format, params object[] args)
	{
		if (IsConsole) ConsoleUtils.WriteWarning(format, args);
		else Logger.LogWarning(format, args);
	}

	#endregion Helpers
}