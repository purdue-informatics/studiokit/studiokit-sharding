using System;

namespace StudioKit.Sharding.Exceptions;

public class ShardException : Exception
{
	public ShardException()
	{
	}

	public ShardException(string message)
		: base(message)
	{
	}

	public ShardException(string message, Exception innerException)
		: base(message, innerException)
	{
	}
}